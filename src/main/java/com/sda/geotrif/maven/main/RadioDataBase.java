package com.sda.geotrif.maven.main;

import java.util.ArrayList;
import java.util.List;

import com.sda.geotrif.maven.util.InvalidArtistNameException;
import com.sda.geotrif.maven.util.InvalidSongMinutesException;
import com.sda.geotrif.maven.util.InvalidSongNameException;
import com.sda.geotrif.maven.util.InvalidSongSecondsException;
import com.sda.geotrif.maven.util.ScannerInput;
import com.sda.geotrif.maven.util.Validation;

public class RadioDataBase {
	private Song song;
	private ScannerInput scanner = new ScannerInput();
	private Validation valid = new Validation();
	private List<Song> playList = new ArrayList<Song>();
	private int songContor;

	public String addSongName() {
		String checkSongName = scanner.inputSongName();
		return checkSongName;
	}

	public String addArtistName() {
		String checkArtistName = scanner.inputArtistName();
		return checkArtistName;
	}

	public int addSongMinutes() {
		int checkSongMinutes = scanner.inputSongMinutes();
		return checkSongMinutes;
	}

	public int addSongSeconds() {
		int checkSongSeconds = scanner.inputSongSeconds();
		return checkSongSeconds;
	}

	public List addPlaylist() {
		String songName = addSongName();
		String artistName = addArtistName();
		int songMinutes = addSongMinutes();
		int songSeconds = addSongSeconds();

		if (!(songName.equals("")) && !(artistName.equals("")) && songMinutes >= 0 && songSeconds >= 0) {
			song = new Song(songName, artistName, songMinutes, songSeconds);
			playList.add(song);
			songContor++;
			System.out.println("Song added.");
		}
		return playList;
	}

	public void printPlaylist() {

		System.out.println("Songs added to playlist: " + songContor);

		int playListHours = 0;
		int playlistMinutes = 0;
		int playListSeconds = 0;

		for (Song song : playList) {
			System.out.println(song);
			playlistMinutes += song.getMinutes();
			playListSeconds += song.getSeconds();
		}

		playListHours = playlistMinutes / 60;
		playlistMinutes = playlistMinutes % 60 + playListSeconds / 60;
		playListSeconds = playListSeconds % 60;

		System.out
				.println("Playlist length: " + playListHours + "h " + playlistMinutes + "m " + playListSeconds + "s.");

	}

}
