package com.sda.geotrif.maven.main;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.sda.geotrif.maven.util.ScannerInput;

/*
 * 2.
Online Radio Database

Create an online radio station database. It should keep information about all added songs. On the first line you 
are going to get the number of songs you are going to try adding. On the next lines you will get the songs to be added 
in the format <artist name>;<song name>;<minutes:seconds>. To be valid, every song should have an artist name, a song name and length. 
Design a custom exception hierarchy for invalid songs: 
InvalidSongException
    InvalidArtistNameException
    InvalidSongNameException
    InvalidSongLengthException
        InvalidSongMinutesException
        InvalidSongSecondsException

Validation
Artist name should be between 3 and 20 symbols. 
Song name should be between 3 and 30 symbols. 
Song length should be between 0 second and 14 minutes and 59 seconds.
Song minutes should be between 0 and 14.
Song seconds should be between 0 and 59.

Check validity in the order artist name -> song name -> song length
If the song is added, print "Song added.". If you can't add a song, print an appropriate exception message. 
On the last two lines print the number of songs added and the total length of the playlist in format {Playlist length: 0h 7m 47s}.
 */

public class Main {

	public static boolean flag = false;
	public static int songNumber;
	public static RadioDataBase radio = new RadioDataBase();
	public static Song song = new Song();
	public static Scanner input = new Scanner(System.in);
	public static List<Song> songPlaylist = new ArrayList<Song>();

	public static void main(String[] args) {

		printMenu();

		while (!flag) {
			choiceSelection();
		}
	}

	public static void printMenu() {
		System.out.println("Song DataBase Menu");
		System.out.println("Press:");
		System.out.println("\t1 -> Number of songs you wish to add.");
		System.out.println("\t2 -> Add song.");
		System.out.println("\t3 -> Print playlist.");
		System.out.println("\t4 -> Print menu.");
		System.out.println("\t5 -> Exit application.");
	}

	public static void choiceSelection() {
		ScannerInput scanner = new ScannerInput();
		int choice = scanner.inputChoiceMenu();

		switch (choice) {
		case 1:
			int numberOfSongs = scanner.inputNumberSongs();
			if (numberOfSongs > 0) {
				songNumber = numberOfSongs;
			} else {
				System.out.println("Please enter a valid number of songs.");
			}
			break;
		case 2:
			int contor = 0;
			while (contor < songNumber) {
				radio.addPlaylist();
				contor++;
			}
			break;
		case 3:
			radio.printPlaylist();
			break;
		case 4:
			printMenu();
			break;
		case 5:
			flag = true;
			System.out.println("Goodbye!");
			break;
		default:
			System.out.println("Please enter a valid menu choice.");
		}

	}

}
