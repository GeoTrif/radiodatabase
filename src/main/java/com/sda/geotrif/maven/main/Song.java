package com.sda.geotrif.maven.main;

import java.util.ArrayList;
import java.util.List;

public class Song {

	private String name;
	private String artistName;
	private int minutes;
	private int seconds;

	public Song() {
	}

	public Song(String name, String artistName, int minutes, int seconds) {
		this.name = name;
		this.artistName = artistName;
		this.minutes = minutes;
		this.seconds = seconds;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}

	public String getArtistName() {
		return this.artistName;
	}

	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}

	public int getMinutes() {
		return this.minutes;
	}

	public void setSeconds(int seconds) {
		this.seconds = seconds;
	}

	public int getSeconds() {
		return this.seconds;
	}

	@Override
	public String toString() {
		return "Song[name = " + this.name + ",artist = " + this.artistName + " ,length = " + this.minutes + " : "
				+ this.seconds + "]";
	}

}
