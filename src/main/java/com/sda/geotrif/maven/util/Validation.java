package com.sda.geotrif.maven.util;

import com.sda.geotrif.maven.main.Song;

public class Validation {

	public boolean isValidSongName(String songName) {
		if (songName.length() >= 3 && songName.length() <= 30) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isValidArtistName(String artistName) {
		if (artistName.length() >= 3 && artistName.length() <= 20) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isValidLength(int minutes, int seconds) {
		if (minutes >= 0 && minutes <= 14 && seconds >= 0 && minutes <= 59) {
			return true;
		} else {
			return false;
		}

	}

	public boolean minutesValidator(int minutes) {
		if (minutes >= 0 && minutes <= 14) {
			return true;
		} else {
			return false;
		}
	}

	public boolean secondsValidator(int seconds) {
		if (seconds >= 0 && seconds <= 59) {
			return true;
		} else {
			return false;
		}
	}
}
