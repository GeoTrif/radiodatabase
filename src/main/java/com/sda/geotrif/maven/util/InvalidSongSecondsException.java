package com.sda.geotrif.maven.util;

public class InvalidSongSecondsException extends InvalidSongLengthException {

	public InvalidSongSecondsException() {
		super();
	}

	public InvalidSongSecondsException(String msg) {
		super(msg);
	}

}
