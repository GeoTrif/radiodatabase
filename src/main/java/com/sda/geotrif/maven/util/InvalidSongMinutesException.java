package com.sda.geotrif.maven.util;

public class InvalidSongMinutesException extends InvalidSongLengthException {

	public InvalidSongMinutesException() {
		super();
	}

	public InvalidSongMinutesException(String msg) {
		super(msg);
	}

}
