package com.sda.geotrif.maven.util;

public class InvalidArtistNameException extends InvalidSongException {

	public InvalidArtistNameException() {
		super();
	}

	public InvalidArtistNameException(String msg) {
		super(msg);
	}

}
