package com.sda.geotrif.maven.util;

public class InvalidSongLengthException extends InvalidSongException {

	public InvalidSongLengthException() {
		super();
	}

	public InvalidSongLengthException(String msg) {
		super(msg);
	}

}
