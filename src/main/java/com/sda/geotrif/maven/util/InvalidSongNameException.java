package com.sda.geotrif.maven.util;

public class InvalidSongNameException extends InvalidSongException {

	public InvalidSongNameException() {
		super();
	}

	public InvalidSongNameException(String msg) {
		super(msg);
	}

}
