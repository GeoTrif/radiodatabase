package com.sda.geotrif.maven.util;

import java.util.Scanner;

public class ScannerInput {
	public Scanner input = new Scanner(System.in);
	public Validation validation = new Validation();

	public int inputChoiceMenu() {
		System.out.println("Enter your choice(4 to print menu):");
		int choice = input.nextInt();
		return choice;
	}

	public int inputNumberSongs() {
		System.out.println("Enter the number of songs you want to add to your database:");
		int songNumber = input.nextInt();
		return songNumber;
	}

	public String inputSongName() {
		System.out.println("Enter the name of the song:");

		try {
			String songName = input.next();
			if (validation.isValidSongName(songName) == true) {
				return songName;
			} else {
				throw new InvalidSongException("Song name should be between 3 and 30 characters.");
			}

		} catch (InvalidSongNameException e) {
			System.out.println("Song name should be between 3 and 30 characters.");
		}
		return "";
	}

	public String inputArtistName() {
		System.out.println("Enter the artist name:");

		try {
			String artistName = input.next();
			if (validation.isValidArtistName(artistName) == true) {
				return artistName;
			} else {
				throw new InvalidArtistNameException("Artist name should be between 3 and 20 characters.");
			}

		} catch (InvalidArtistNameException e) {
			System.out.println("Artist name should be between 3 and 20 characters.");
		}
		return "";
	}

	public int inputSongMinutes() {

		System.out.println("Enter the minutes of the song:");

		try {
			int songMinutes = input.nextInt();

			if (validation.minutesValidator(songMinutes) == true) {
				return songMinutes;
			} else {
				throw new InvalidSongMinutesException("Song minutes should be between 0 and 14.");
			}
		} catch (InvalidSongMinutesException e) {

			System.out.println("Song minutes should be between 0 and 14.");
		}
		return 0;
	}

	public int inputSongSeconds() {
		System.out.println("Enter the seconds of the song:");

		try {
			int songSeconds = input.nextInt();

			if (validation.secondsValidator(songSeconds) == true) {
				return songSeconds;
			} else {
				throw new InvalidSongSecondsException("Song seconds should be between 0 and 59.");
			}

		} catch (InvalidSongSecondsException e) {
			System.out.println("Song seconds should be between 0 and 59.");
		}
		return 0;
	}

}
