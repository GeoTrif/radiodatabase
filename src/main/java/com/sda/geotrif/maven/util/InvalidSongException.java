package com.sda.geotrif.maven.util;

public class InvalidSongException extends RuntimeException {

	public InvalidSongException() {
		super();
	}

	public InvalidSongException(String msg) {
		super(msg);
	}
}
